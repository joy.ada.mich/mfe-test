import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import App from './src/App';
import Pos from './src/Pos';

export default class Root extends Component {
  render() {
    return (
      <Router>
        <Route path="/" component={App} />
        <Route exact path="/appcra/pos" component={Pos} />
      </Router>
    );
  }
}
