const path = require('path');

module.exports = function override(config, env) {
  config.output = {
    ...config.output
    // library: 'reactApp',
    // libraryTarget: 'window'
    // publicPath: '/dist/',
    // path: path.resolve(__dirname, 'dist')
  };
  return config;
};
