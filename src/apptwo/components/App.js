import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div>
        <div className="container">Hello from React App Two</div>
      </div>
    );
  }
}

export default App;
