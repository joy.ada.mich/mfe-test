import { registerApplication, start } from 'single-spa';

registerApplication(
  'appone',
  () => import('./src/appone/main.app.js'),
  // () => (location.pathname === '/apptwo' || location.pathname === '/appcra' ? false : true)
  () => (location.pathname.includes('/appone') || location.pathname === '/' ? true : false)
);

registerApplication(
  'apptwo',
  () => import('./src/apptwo/main.app.js'),
  // () => (location.pathname === '/appone' || location.pathname === '/appcra' ? false : true)
  () => (location.pathname.includes('/apptwo') || location.pathname === '/' ? true : false)
);

registerApplication(
  'appcra',
  () => import('./src/appcra/main.app.js'),
  // () => (location.pathname === '/appone' || location.pathname === '/apptwo' ? false : true)
  () => (location.pathname.includes('/appcra') || location.pathname === '/' ? true : false)
);

start();
